Hello, I'm Aria. I'm a cs student interested in infrastructure, software engineering, and how programming languages can make these things better.
I use she/her pronouns.

If you want to know what I'm working on, check my [website](https://aria.rip).
